package dam.android.alex.u4t6contacts;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;

import java.util.ArrayList;

public class MyContacts {
    private  ArrayList<ContactItem> myDataSet;
    private  Context context;

    public MyContacts( Context context) {
        this.context = context;
        this.myDataSet = getContacts();

    }

    private ArrayList<ContactItem> getContacts(){
        // Todo cambiamos el array list de String
        //  a de tipo ContactItem

        ArrayList<ContactItem> contactsList=new ArrayList<>();


        ContentResolver contentResolver= context.getContentResolver();

        String[] projection=new String[]{
                //TODO obtenemos la informacion que queremos saber sobre los Contactos
                ContactsContract.Data._ID,
                ContactsContract.Data.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.Data.CONTACT_ID,
                ContactsContract.Data.LOOKUP_KEY,
                ContactsContract.Data.RAW_CONTACT_ID,
                ContactsContract.CommonDataKinds.Phone.TYPE,
                ContactsContract.Data.PHOTO_THUMBNAIL_URI
             };
            //TODO Crea la consulta
        String selectionFilter=ContactsContract.Data.MIMETYPE+"='" +
                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE+"' AND " +
                ContactsContract.CommonDataKinds.Phone.NUMBER+" IS NOT NULL";

        Cursor contactsCursor=contentResolver.query(ContactsContract.Data.CONTENT_URI,
                projection,
                selectionFilter,
                null,
                ContactsContract.Data.DISPLAY_NAME+" ASC");

        if (contactsCursor != null){
            //Todo obtenemos el indice de la informacion que queremos obtener del contacto
            int idIndex=contactsCursor.getColumnIndexOrThrow(ContactsContract.Data._ID);
            int nameIndex=contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.DISPLAY_NAME);
            int numberIndex=contactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int idContacto=contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.CONTACT_ID);
            int lookup=contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.LOOKUP_KEY);
            int rawContact=contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.RAW_CONTACT_ID);
            int typo=contactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.TYPE);
            int imagenIndex=contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.PHOTO_THUMBNAIL_URI);




            while (contactsCursor.moveToNext()){
                //TODO Obtenemos los datos del Contacto
                String id=contactsCursor.getString(idIndex);
                String name=contactsCursor.getString(nameIndex);
                String number=contactsCursor.getString(numberIndex);
                String idContactos=contactsCursor.getString(idContacto);
                String lookups=contactsCursor.getString(lookup);
                String raw=contactsCursor.getString(rawContact);
                String typos=contactsCursor.getString(typo);
                String imagen=contactsCursor.getString(imagenIndex);

                //TODO le pasamos la informacion que obtenemos anteriormente  al constructor

                contactsList.add(new ContactItem(name,number,idContactos,imagen,typos,id,raw,lookups));

            }
            contactsCursor.close();//TODO Cerramos el Cursor

        }
        return contactsList;
    }

    public ContactItem getContactData(int position){
        return myDataSet.get(position);
    }
    public int getCount(){
        return myDataSet.size();
    }
}
