package dam.android.alex.u4t6contacts;

public class ContactItem {
    String name;
    String number;
    String idContactos;
    String imagen;
    String typos;
    String id;
    String raw;
    String lookups;

    public ContactItem(String name, String number, String idContactos, String imagen, String typos, String id, String raw, String lookups) {//TODO Constructor
        this.name = name;
        this.number = number;
        this.idContactos = idContactos;
        this.imagen = imagen;
        //TODO En el typo nos aparecera segun el tipo de contacto que es
        switch (typos){
            case "1":
                this.typos = "HOME";
                break;
            case "2":
                this.typos= "WORK";
                break;
            case "3":
                this.typos= "MOBILE";
                break;
            default:
                this.typos="OTHER";
                break;

        }

        this.id = id;
        this.raw = raw;
        this.lookups = lookups;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getIdContactos() {
        return idContactos;
    }

    public void setIdContactos(String idContactos) {
        this.idContactos = idContactos;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getTypos() {
        return typos;
    }

    public void setTypos(String typos) {
        this.typos = typos;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRaw() {
        return raw;
    }

    public void setRaw(String raw) {
        this.raw = raw;
    }

    public String getLookups() {
        return lookups;
    }

    public void setLookups(String lookups) {
        this.lookups = lookups;
    }
}
