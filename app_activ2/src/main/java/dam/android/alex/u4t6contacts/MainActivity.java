package dam.android.alex.u4t6contacts;


import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.animation.Animator;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements MyAdapter.onItemClickListener, View.OnScrollChangeListener, MyAdapter.onLongListener {
    MyContacts myContacts;
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;


    CardView cardView;

    TextView loockup;
    TextView nombre;
    TextView contacto;
    TextView typo;
    TextView idcard;
    TextView idContacto;
    TextView rawContactoid;


    private static String[] PERMISSIONS_CONTACTS = {Manifest.permission.READ_CONTACTS};
    private static final int REQUEST_CONTACTS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
         //TODO Llamamos el siguiente metodo
        introducirCardView();



    }

    private void setUI() {
        recyclerView = findViewById(R.id.recyclerViewContacts);
        recyclerView.setHasFixedSize(true);

        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                DividerItemDecoration.VERTICAL);

        recyclerView.addItemDecoration(mDividerItemDecoration);

        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        //TODO Cuando se realiza el scroll en la actividad
        recyclerView.setOnScrollChangeListener(this);


    }

    private void setListAdapter() {
        myContacts = new MyContacts(this);
        recyclerView.setAdapter(new MyAdapter(myContacts, this, this));


        if (myContacts.getCount() > 0) {
            findViewById(R.id.tvEmptyList).setVisibility(View.INVISIBLE);
            adapter = new MyAdapter(myContacts, this, this);
            recyclerView.setAdapter(adapter);

        }


    }


    private boolean chekPermissions() {
            // Obtengo si tiene los permisos necesarios de contactos
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, MainActivity.PERMISSIONS_CONTACTS, MainActivity.REQUEST_CONTACTS);
            return false;
        } else
            return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                setListAdapter();
            } else {
                Toast.makeText(this, getString(R.string.contacts_read_right_required), Toast.LENGTH_LONG).show();
            }

        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }

    @Override
    public void onItemClick(ContactItem activiyName) {
        // TODO Le adjudicamos la informacion al testview
        loockup.setText(activiyName.getLookups());
        nombre.setText(activiyName.getName());
        contacto.setText(activiyName.getNumber());
        typo.setText(activiyName.getTypos());
        idcard.setText("_ID: " + activiyName.getId());
        idContacto.setText("CONTACT_ID: " + activiyName.getIdContactos());
        rawContactoid.setText("RAW_CONTACT_ID: " + activiyName.getRaw());

        //Todo ponermos visible el cardview
        cardView.setVisibility(View.VISIBLE);
        //TODO Creo el animator le pasamos la cardview y los datos  necesarios para que inicie la animacion
        Animator delayAnimator = ViewAnimationUtils.createCircularReveal(cardView, 600, 200, 20, 400);
        //TODO Le decimos el tiempo que tiene que esperar para iniciarlo
        delayAnimator.setDuration(250);
        //Todo Lo iniciamos
        delayAnimator.start();


    }

    public void introducirCardView() {//TODO Pillamos asignamos cada objeto a su respectivo objeto
        this.cardView = findViewById(R.id.cardviewMain);

        this.loockup = findViewById(R.id.lookupkey);
        this.nombre = findViewById(R.id.nombreCardContacto);
        this.contacto = findViewById(R.id.NumeroCardContacto);
        this.typo = findViewById(R.id.TypoCardContacto);
        this.idcard = findViewById(R.id.idCard);
        this.idContacto = findViewById(R.id.idContactoCard);
        this.rawContactoid = findViewById(R.id.RawContactId);

        //TODO Ponemos la cardview invisible

        cardView.setVisibility(View.INVISIBLE);


    }


    @Override
    public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        //TODO Cuando hacemos scroll hacia arriba o hacia abajo  lo vueleve a esconder
        // o lo pone invisible


        this.cardView.setVisibility(View.INVISIBLE);

    }

    @Override //TODO Cuando mantenemos pulsado se no abre para poder modificar el contacto
    public boolean onLongClick(ContactItem activiyName) {
        //TODO le decimos que la cardview sea invisible para que no la muestre
        cardView.setVisibility(View.INVISIBLE);
        //TODO Lo que hace es que creamos un intent para que nos muestre la informacion de ese usuario
        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, activiyName.getIdContactos());
        //TODO Le ponemos la uri al intent y le decimos que inicie la activity
        // a la uri se le pasa la id del contacto y le pasas los contactos
        intent.setData(uri);
        startActivity(intent);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Todo comprueba si estan los permisos
        if (chekPermissions())
            //Todo se modifica la informacion de los contactos  cuando acaba el ciclo  en el caso de que se haya modificado
            setListAdapter();

    }
}