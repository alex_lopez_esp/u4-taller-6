package dam.android.alex.u4t6contacts;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>  {
    private MyContacts myContacts;
    private  onItemClickListener listener;
    private onLongListener listenerLong;



    static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView id;
        TextView nombre;
        TextView contacto;
        View cardView;



        public MyViewHolder(CardView view) {
            // TODO Le adjudicamos la informacion al cardview
            super(view);
            this.id=view.findViewById(R.id.idContacto);
            this.nombre=view.findViewById(R.id.nombreContacto);
            this.contacto=view.findViewById(R.id.numeroContacto);
            this.imageView=view.findViewById(R.id.imagenContacto);
            this.cardView=view;


        }

        public void bind(ContactItem contactData, onItemClickListener listener,onLongListener longListener){
            //Todo le ponemos al la informacion al Cardview ,
            // la id , el nombre, el contacto y la imagen
            this.id.setText(contactData.getId());

            this.nombre.setText(contactData.getName());
            this.contacto.setText(contactData.getNumber());


            if (contactData.getImagen() == null){
                this.imageView.setImageResource(R.drawable.descarga);
            }else {
                //Todo en el caso de que sea nulo se le adjudica una
                this.imageView.setImageURI(Uri.parse(contactData.getImagen()));
            }

            //TODO Lo que hace es que inicia el OnClickListener cuando pulsemos un contacto
            this.cardView.setOnClickListener(v -> listener.onItemClick(contactData));

            //TODO Lo que hace es que inicia el OnLongClickListener  cuando mantenemos pulsado  un contacto
            this.cardView.setOnLongClickListener(v -> longListener.onLongClick(contactData));




        }
    }

    MyAdapter(MyContacts myContacts, onItemClickListener listener,onLongListener longlistener){
        //TODO adjudicamos la informacion en el Constructor
        this.listenerLong = longlistener;
        this.myContacts=myContacts;
        this.listener=listener;
    }



    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {//TODO Lo cambiamos por un ardView
        CardView tv=(CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview,parent,false);

        return new MyViewHolder(tv);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        //TODO Le pasamos los datos del contacto , el OnClickListener y el OnLongClickListener
        viewHolder.bind( myContacts.getContactData(position),listener,listenerLong);


    }

    @Override
    public int getItemCount() {
        return myContacts.getCount();
    }


    //TODO Creamos el OnItemClickListener
    public interface onItemClickListener{
        void onItemClick(ContactItem activiyName);




    }
    //TODO Creamos el onLongListener
    // para que cuando se mantenga pulsado sobre el contacto
    public interface onLongListener{
        boolean onLongClick(ContactItem activiyName);




    }


}
