package dam.android.alex.u4t6contacts;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    MyContacts myContacts;
    RecyclerView recyclerView;
    RecyclerView.Adapter adapter;






    private static  String[] PERMISSIONS_CONTACTS={Manifest.permission.READ_CONTACTS};
    private static final int REQUEST_CONTACTS=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();

        if (chekPermissions())
            setListAdapter();

    }

    private void setUI(){
        recyclerView=findViewById(R.id.recyclerViewContacts);
        recyclerView.setHasFixedSize(true);

        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
               DividerItemDecoration.VERTICAL);

        recyclerView.addItemDecoration(mDividerItemDecoration);


        recyclerView.setLayoutManager(new LinearLayoutManager(this,RecyclerView.VERTICAL,false));
    }

    private void setListAdapter(){
        myContacts=new MyContacts(this);
        recyclerView.setAdapter(new MyAdapter(myContacts));

        if (myContacts.getCount() > 0)
            findViewById(R.id.tvEmptyList).setVisibility(View.INVISIBLE);
            adapter=new MyAdapter(myContacts);
            recyclerView.setAdapter(adapter);

    }


    private boolean chekPermissions(){
        if (ActivityCompat.checkSelfPermission(this,Manifest.permission.READ_CONTACTS)!= PackageManager.PERMISSION_GRANTED){

            ActivityCompat.requestPermissions(this,MainActivity.PERMISSIONS_CONTACTS,MainActivity.REQUEST_CONTACTS);
            return false;
        }else
            return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,  String[] permissions,  int[] grantResults){
        if (requestCode==REQUEST_CONTACTS){
            if (grantResults[0]== PackageManager.PERMISSION_GRANTED) {
                setListAdapter();
            }else {
                Toast.makeText(this, getString(R.string.contacts_read_right_required), Toast.LENGTH_LONG).show();
            }

        }else{
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }

    }
}