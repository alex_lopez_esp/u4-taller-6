package dam.android.alex.u4t6contacts;

public class ContactItem {
    String id;
    String nombreContacto;
    String numerotelefono;
    String fotocontacto;

    public ContactItem(String id, String nombreContacto, String numerotelefono, String fotocontacto) {
        this.id = id;
        this.nombreContacto = nombreContacto;
        this.numerotelefono = numerotelefono;
        this.fotocontacto = fotocontacto;
    }

    public ContactItem(String id, String nombreContacto, String numerotelefono) {
        this.id = id;
        this.nombreContacto = nombreContacto;
        this.numerotelefono = numerotelefono;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombreContacto() {
        return nombreContacto;
    }

    public void setNombreContacto(String nombreContacto) {
        this.nombreContacto = nombreContacto;
    }

    public String getNumerotelefono() {
        return numerotelefono;
    }

    public void setNumerotelefono(String numerotelefono) {
        this.numerotelefono = numerotelefono;
    }

    public String getFotocontacto() {
        return fotocontacto;
    }

    public void setFotocontacto(String fotocontacto) {
        this.fotocontacto = fotocontacto;
    }
}
