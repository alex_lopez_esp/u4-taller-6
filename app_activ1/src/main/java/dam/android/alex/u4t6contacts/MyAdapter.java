package dam.android.alex.u4t6contacts;

import android.graphics.drawable.Icon;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private MyContacts myContacts;


    static class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView id;
        TextView nombre;
        TextView contacto;

        public MyViewHolder(CardView view) {
          super(view);
            this.id=view.findViewById(R.id.idContacto);
            this.nombre=view.findViewById(R.id.nombreContacto);
            this.contacto=view.findViewById(R.id.numeroContacto);
            this.imageView=view.findViewById(R.id.imagenContacto);



        }
        public void bind(ContactItem contactData){
            this.id.setText(contactData.getId());
            this.nombre.setText(contactData.getNombreContacto());
            this.contacto.setText(contactData.getNumerotelefono());

            if (contactData.getFotocontacto() == null){
                this.imageView.setImageResource(R.drawable.descarga);
            }else {
                this.imageView.setImageURI(Uri.parse(contactData.getFotocontacto()));
            }





        }
    }

    MyAdapter(MyContacts myContacts){
        this.myContacts=myContacts;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CardView tv=(CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview,parent,false);

        return new MyViewHolder(tv);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {
        viewHolder.bind( myContacts.getContactData(position));

    }

    @Override
    public int getItemCount() {
        return myContacts.getCount();
    }


}
