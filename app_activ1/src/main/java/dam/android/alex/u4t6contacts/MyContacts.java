package dam.android.alex.u4t6contacts;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;

import java.util.ArrayList;

public class MyContacts {
    private  ArrayList<ContactItem> myDataSet;
    private  Context context;

    public MyContacts( Context context) {
        this.context = context;
        this.myDataSet = getContacts();

    }

    private ArrayList<ContactItem> getContacts(){

        ArrayList<ContactItem> contactsList=new ArrayList<>();

        //Access to ContentProviders
        ContentResolver contentResolver= context.getContentResolver();

        String[] projection=new String[]{ContactsContract.Data._ID,
                ContactsContract.Data.CONTACT_ID,
                ContactsContract.Data.RAW_CONTACT_ID,
                ContactsContract.CommonDataKinds.Phone.TYPE,
                ContactsContract.Data.PHOTO_THUMBNAIL_URI,
                ContactsContract.Data.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER,
             };

        String selectionFilter=ContactsContract.Data.MIMETYPE+"='" +
                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE+"' AND " +
                ContactsContract.CommonDataKinds.Phone.NUMBER+" IS NOT NULL";

        Cursor contactsCursor=contentResolver.query(ContactsContract.Data.CONTENT_URI,
                projection,
                selectionFilter,
                null,
                ContactsContract.Data.DISPLAY_NAME+" ASC");

        if (contactsCursor != null){

            int nameIndex=contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.DISPLAY_NAME);
            int numberIndex=contactsCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int idIndex=contactsCursor.getColumnIndexOrThrow(ContactsContract.Data._ID);
            int imagenIndex=contactsCursor.getColumnIndexOrThrow(ContactsContract.Data.PHOTO_THUMBNAIL_URI);


            while (contactsCursor.moveToNext()){

                String name=contactsCursor.getString(nameIndex);
                String number=contactsCursor.getString(numberIndex);
                String id=contactsCursor.getString(idIndex);
                String imagen=contactsCursor.getString(imagenIndex);

                contactsList.add(new ContactItem(id,name,number,imagen));

            }
            contactsCursor.close();

        }
        return contactsList;
    }

    public ContactItem getContactData(int position){
        return myDataSet.get(position);
    }
    public int getCount(){
        return myDataSet.size();
    }
}
